import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "@reduxjs/toolkit";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import {
  connectRouter,
  ConnectedRouter,
  routerMiddleware,
} from "connected-react-router";
import thunk from "redux-thunk";
import rootReducer from "./app/redux/index";
import { history } from "./app/navigation/history";
import App from "./App";
import "./index.css";

export const store = createStore(
  connectRouter(history)(rootReducer),
  applyMiddleware(routerMiddleware(history), thunk)
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
