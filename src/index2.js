/* import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./app/redux/index";
import {
  connectRouter,
  ConnectedRouter,
  routerMiddleware,
} from "connected-react-router";
import { history } from "./app/navigation/history";

export const store = createStore(
  connectRouter(history)(rootReducer),
  applyMiddleware(routerMiddleware(history), thunk)
);

//console.log(process.env.REACT_APP_TEST === undefined ? "echec du test de passage d'argument en parametre ": "T'es le best : " + process.env.REACT_APP_TEST)
//console.log("SENDMP_API_URL : ",process.env.REACT_APP_SENDMP_API)
//console.log("AGILE_API_URL : ",process.env.REACT_APP_AGILE_PROJECT_API)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
 */
