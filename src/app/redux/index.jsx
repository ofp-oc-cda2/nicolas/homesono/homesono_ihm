import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import { history } from "../navigation/history";
import { usersReducer } from "./users/usersReducer";
import { productsReducer } from "./products/productsReducer";
import {categorieReducer} from './categorie/categorieReducer'

const rootReducer = combineReducers({
  productsReducer,
	categorieReducer,
  usersReducer,
  router: connectRouter(history),
});

export default rootReducer;
