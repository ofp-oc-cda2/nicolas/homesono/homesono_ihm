import * as type from "./productsType";

const initialState = {
  product: [],
  errorProduct: "",
  isLoadingProduct: false,

  selectedProduct: "",
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.SET_PRODUCT:
      return { ...state, isLoadingProduct: true };
    case type.SET_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoadingProduct: false,
        product: action.payload,
        errorProduct: "",
      };
    case type.SET_PRODUCT_ERROR:
      return {
        ...state,
        isLoadingProduct: false,
        product: [],
        errorProduct: action.payload,
      };

    case type.SELECT_PRODUCT:
      return { ...state, selectedProduct: action.payload };

    default:
      return state;
  }
};

export default productsReducer;
