import * as types from "./productsType";
import { HomesonoAPI } from "../../util/WsCaller";

export const setProduct = () => ({
  type: types.SET_PRODUCT,
});
export const setProductSuccess = (data) => ({
  type: types.SET_PRODUCT_SUCCESS,
  payload: data,
});
export const setProductError = (data) => ({
  type: types.SET_PRODUCT_ERROR,
  payload: data,
});
export const selectProduct = (data) => ({
  type: types.SELECT_PRODUCT,
  payload: data,
});

//=================================================================
//=========================== MIDDLEWARE ==========================
//=================================================================

export const getProducts = () => (dispatch) => {
  dispatch(setProduct());
  HomesonoAPI.get("/products")
    .then((res) => {
      dispatch(setProductSuccess(res.data));
    })
    .catch((err) => {
      /* 		dispatch(removeAuth(err.response.status))
       */ dispatch(setProductError(err.data));
    });
};
