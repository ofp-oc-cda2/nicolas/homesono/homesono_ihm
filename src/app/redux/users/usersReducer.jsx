import * as type from "./usersType";

const initialState = {
  user: [],
  errorUser: "",
  isLoadingUser: false,
};

export const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.SET_USER:
      return { ...state, isLoadingUser: true };
    case type.SET_USER_SUCCESS:
      return {
        ...state,
        isLoadingUser: false,
        user: action.payload,
        errorUser: "",
      };
    case type.SET_USER_ERROR:
      return {
        ...state,
        isLoadingUser: false,
        user: [],
        errorUser: action.payload,
      };
    case type.SET_DELETE_USER:
      return {
        ...state,
        isLoadingUser: true,
      };
    case type.SET_DELETE_USER_SUCCESS:
      return {
        ...state,
        user: state.user.filter(
          (e) => e.usrIdUsrPK !== action.payload.usrIdUsrPK
        ),
      };

    default:
      return state;
  }
};

export default usersReducer;
