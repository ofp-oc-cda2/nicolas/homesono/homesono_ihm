import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import * as productsAction from "../../redux/products/productsAction";
import Product from "../product/product";
import { Link } from "react-router-dom";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  useRouteMatch,
  useParams,
} from "react-router-dom";
import "./products.scss";

function ProductsComponent({ stateProducts, actionProducts }) {
  let match = useRouteMatch();

  useEffect(() => {
    actionProducts.getProducts();
  }, []);

  const chooseValue = () => {
    return stateProducts.product;
  };

  const dynamicColumn = () => {
    return colExemple.map((e) => {
      if (
        e.field === "proBookingBkgFK" ||
        e.field === "proIdProPK" ||
        e.field === "proInstructionsPro"
      ) {
        return <></>;
      }
      return <Column key={e.key} field={e.field} header={e.key} />;
    });
  };

  const dynamicLink = () => {
    return chooseValue().map((e) => {
      return (
        <li key={e.proIdProPK} onClick={() => {}}>
          {e.proNamePro} : {e.proIdProPK}
          <button
            onClick={() => {
              actionProducts.selectProduct(e);
            }}
          >
            <NavLink to="/product">voir</NavLink>
          </button>
        </li>
      );
    });
  };

  const button = (row) => {
    return (
      <button
        onClick={() => {
          actionProducts.selectProduct(row);
        }}
      >
        <NavLink to="/product">voir</NavLink>
      </button>
    );
  };

  let colExemple = [];
  if (chooseValue().length > 0) {
    let n = 0;
    for (let key of Object.keys(chooseValue()[0])) {
      colExemple.push({
        key: key,
        field: key,
      });
    }
  }

  return (
    <>
      <div className="products">
        <button
          onClick={() => {
            console.log(stateProducts.selectedProduct.proNamePro);
          }}
        >
          STATE
        </button>
        <div className="heading">
          <h1>produits</h1>
        </div>
        <div className="ensemble">
          <ul>{dynamicLink()}</ul>
        </div>
        <DataTable value={chooseValue()}>
          {dynamicColumn()}
          <Column body={button}></Column>
        </DataTable>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  state: state,
  stateProducts: state.productsReducer,
});

const mapDispatchToProps = (dispatch) => ({
  actionProducts: bindActionCreators(productsAction, dispatch),
});

const Products = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsComponent);
export default Products;
