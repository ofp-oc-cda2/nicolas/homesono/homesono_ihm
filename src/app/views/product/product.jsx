import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as productsAction from "../../redux/products/productsAction";
import { Link } from "react-router-dom";

import "./product.scss";

function ProductComponent({ stateProducts, action }) {
  useEffect(() => {}, []);

  return (
    <>
      <div className="navigation">
        <nav>
          <Link to="/products">Retour aux produits</Link>
        </nav>
      </div>
      <div className="categorie ">
        <h1>Produit unique</h1>
        <h3>ID du produit: {stateProducts.selectedProduct.proIdProPK}</h3>
        <h3>Nom du produit: {stateProducts.selectedProduct.proNamePro}</h3>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  state: state,
  stateProducts: state.productsReducer,
});

const mapDispatchToProps = (dispatch) => ({
  action: bindActionCreators(productsAction, dispatch),
});

const Product = connect(mapStateToProps, mapDispatchToProps)(ProductComponent);
export default Product;
