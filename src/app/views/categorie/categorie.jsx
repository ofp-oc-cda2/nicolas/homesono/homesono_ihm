import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";
import * as categorieAction from "../../redux/categorie/categorieAction";

//import * as Action from "../../redux/";
import "./categorie.scss";

function CategorieComponent({ cat, stateCategorie, actionCategorie }) {
  useEffect(() => {}, []);

  return (
    <>
      <button
        onClick={() => {
          actionCategorie.selectCategory("");
        }}
      >
        fermer la catégorie
      </button>
      <div className="categorie ">
        <h1>Catégorie</h1>
        <h3>ID de la catégorie: {cat.catIdCatPK}</h3>
        <h3>Nom de la catégorie: {cat.catNameCat}</h3>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  state: state,
  stateCategorie: state.categorieReducer,
});

const mapDispatchToProps = (dispatch) => ({
  actionCategorie: bindActionCreators(categorieAction, dispatch),
});

const Categorie = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategorieComponent);
export default Categorie;
