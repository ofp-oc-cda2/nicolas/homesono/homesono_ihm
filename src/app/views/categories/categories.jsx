import React, { useEffect, useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  useRouteMatch,
  useParams,
} from "react-router-dom";
import * as categorieAction from "../../redux/categorie/categorieAction";
import { Dialog } from "primereact/dialog";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import Categorie from "../categorie/categorie";
import "./categories.scss";

function CategoriesComponent({ stateCategorie, actionCategorie }) {
  let match = useRouteMatch();
  const [displayCreateCategory, setDisplayCreateCategory] = useState(false);
  const [displayUpdateCategory, setDisplayUpdateCategory] = useState(false);
  const [displayDeleteCategory, setDisplayDeleteCategory] = useState(false);
  const [position, setPosition] = useState("center");
  const [nameCategory, setNameCategory] = useState("");
  const [idCategory, setIdCategory] = useState("");

  const dialogFuncMap = {
    displayCreateCategory: setDisplayCreateCategory,
    displayUpdateCategory: setDisplayUpdateCategory,
    displayDeleteCategory: setDisplayDeleteCategory,
  };

  useEffect(() => {
    actionCategorie.getCategorieList();
  }, []);

  const chooseValue = () => {
    return stateCategorie.categorie;
  };

  const onClick = (name, position) => {
    dialogFuncMap[`${name}`](true);

    if (position) {
      setPosition(position);
    }
  };

  const openUpdate = (name, position) => {
    dialogFuncMap[`${name}`](true);

    if (position) {
      setPosition(position);
    }
  };
  const openDelete = (name, position) => {
    dialogFuncMap[`${name}`](true);

    if (position) {
      setPosition(position);
    }
  };

  const onHide = (name) => {
    dialogFuncMap[`${name}`](false);
  };

  const renderFooterCreate = (name) => {
    return (
      <div>
        <Button
          label="Créer"
          icon="pi pi-check"
          className="p-button-success"
          onClick={() => {
            actionCategorie.getAddCategorie(nameCategory);
            onHide(name);
          }}
          autoFocus
        />
      </div>
    );
  };
  const renderFooterDelete = (name) => {
    return (
      <div>
        <Button
          label="Suppimer"
          icon="pi pi-trash"
          className="p-button-danger"
          onClick={() => {
            actionCategorie.getDeleteCategorie(idCategory);
            onHide(name);
          }}
          autoFocus
        />
      </div>
    );
  };
  const renderFooterUpdate = (name) => {
    return (
      <div>
        <Button
          label="Modifier"
          className="p-button-info"
          icon="pi pi-pencil"
          onClick={() => {
            actionCategorie.getUpdateCategorie(idCategory, nameCategory);
            onHide(name);
          }}
          autoFocus
        />
      </div>
    );
  };

  const dynamicLink = () => {
    return chooseValue().map((e) => {
      return (
        <li key={e.catIdCatPK} onClick={() => {}}>
          {e.catNameCat} : {e.catIdCatPK}
          {/*         <NavLink
            to={`${match.url}/` + e.catIdCatPK}
            activeStyle={{
              border: "1px solid grey",
              color: "green",
            }}
          >
            {e.catNameCat}
          </NavLink> */}
          <button
            onClick={() => {
              openUpdate("displayUpdateCategory");
              setNameCategory(e.catNameCat);
              setIdCategory(e.catIdCatPK);
            }}
          >
            modifier
          </button>
          <button
            onClick={() => {
              openDelete("displayDeleteCategory");
              setNameCategory(e.catNameCat);
              setIdCategory(e.catIdCatPK);
            }}
          >
            supprimer
          </button>
          <button
            onClick={() => {
              actionCategorie.selectCategory(e);
            }}
          >
            voir
          </button>
        </li>
      );
    });
  };

  let colExemple = [];
  if (chooseValue().length > 0) {
    for (let key of Object.keys(chooseValue()[0])) {
      colExemple.push({
        key: key,
        field: key,
      });
    }
  }

  const singleCategorie = () => {
    if (stateCategorie.selectedCategory === "") {
      return <h3>Sélectionnez une catégorie</h3>;
    } else {
      return <Categorie cat={stateCategorie.selectedCategory} />;
    }
  };

  return (
    <>
      {/* <button
        onClick={() => {
          console.log(stateCategorie.selectedCategory);
        }}
      >
        STATE
      </button> */}

      <div className="categories">
        <div className="heading">
          <h1>Catégories</h1>

          <Dropdown
            value={stateCategorie.selectedCategory}
            options={stateCategorie.categorie}
            onChange={(e) => {
              actionCategorie.selectCategory(e.value);
            }}
            optionLabel="catNameCat"
            placeholder="Sélectionnez une catégorie"
          />

          <Button
            label="Créer une catégorie"
            icon="pi pi-plus"
            className="p-button-success"
            onClick={() => {
              setNameCategory("");
              onClick("displayCreateCategory");
            }}
          />
          <Dialog
            header="nouvelle catégorie"
            visible={displayCreateCategory}
            style={{ width: "50vw" }}
            footer={renderFooterCreate("displayCreateCategory")}
            onHide={() => onHide("displayCreateCategory")}
          >
            <InputText
              value={nameCategory}
              onChange={(e) => setNameCategory(e.target.value)}
            />
          </Dialog>
          <Dialog
            header={"modifier la catégorie " + nameCategory}
            visible={displayUpdateCategory}
            style={{ width: "50vw" }}
            footer={renderFooterUpdate("displayUpdateCategory")}
            onHide={() => onHide("displayUpdateCategory")}
          >
            <InputText
              value={nameCategory}
              onChange={(e) => {
                setNameCategory(e.target.value);
              }}
            />
          </Dialog>
          <Dialog
            header={"supprimer la catégorie " + nameCategory}
            visible={displayDeleteCategory}
            style={{ width: "50vw" }}
            footer={renderFooterDelete("displayDeleteCategory")}
            onHide={() => onHide("displayDeleteCategory")}
          ></Dialog>
        </div>
        <ul>{dynamicLink()}</ul>

        {singleCategorie()}
        {/*         <Switch>
          <Route path={`${match.path}/:categoryId`}>
            <Categorie />
          </Route>
          <Route path={match.path}></Route>
        </Switch> */}
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  state: state,
  stateCategorie: state.categorieReducer,
});

const mapDispatchToProps = (dispatch) => ({
  actionCategorie: bindActionCreators(categorieAction, dispatch),
});

const Categories = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoriesComponent);
export default Categories;
