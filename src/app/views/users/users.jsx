import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import * as usersActions from "../../redux/users/usersAction";
import "./users.scss";
import { getSupportedCodeFixes } from "typescript";
import { Button } from "primereact/button";

function UsersComponent({ stateUsers, actionUsers }) {
  useEffect(() => {
    actionUsers.getUser();
    console.log(stateUsers);
  }, []);

  const chooseValue = () => {
    return stateUsers.user;
  };

  const dynamicColumn = () => {
    return colExemple.map((e) => {
      if (
        e.field === "usrPasswordUsr" ||
        e.field === "usrIdUsrPK" ||
        e.field === "usrZipcodeUsr" ||
        e.field === "usrSignindateUsr" ||
        e.field === "usrEmailUsr"
      ) {
        return <></>;
      }
      return <Column key={e.key} field={e.field} header={e.key} />;
    });
  };

  let colExemple = [];
  if (chooseValue().length > 0) {
    for (let key of Object.keys(chooseValue()[0])) {
      if (key === "usrRoleRolFK") {
        colExemple.push({
          key: key + ".rolNameRol",
          field: key + ".rolNameRol",
        });
      } else {
        colExemple.push({
          key: key,
          field: key,
        });
      }
    }
  }

  const suppr = (row) => {
    return (
      <Button
        icon="pi pi-trash"
        onClick={() => {
          console.log(row.usrIdUsrPK);
          actionUsers.getDeleteUser(row.usrIdUsrPK);
        }}
      ></Button>
    );
  };

  return (
    <>
      <div className="users ">
        <h1>Utilisateurs</h1>
        <DataTable value={chooseValue()}>
          <Column body={(row) => suppr(row)}></Column>
          {dynamicColumn()}
        </DataTable>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  state: state,
  stateUsers: state.usersReducer,
});

const mapDispatchToProps = (dispatch) => ({
  actionUsers: bindActionCreators(usersActions, dispatch),
});

const Users = connect(mapStateToProps, mapDispatchToProps)(UsersComponent);
export default Users;
