import React, { useEffect, useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as categorieAction from "../../redux/categorie/categorieAction";
import "./homepage.scss";

function HomePageComponent({ stateCategorie, actionCategorie }) {
  useEffect(() => {}, []);

  return (
    <>
      <div className="homepage">
        <h1>page d'accueil</h1>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  stateCategorie: state.categorieReducer,
});
const mapDispatchToProps = (dispatch) => ({
  actionCategorie: bindActionCreators(categorieAction, dispatch),
});
const Home = connect(mapStateToProps, mapDispatchToProps)(HomePageComponent);

export default Home;
