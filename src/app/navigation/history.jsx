import { createBrowserHistory } from "history";

export const history = createBrowserHistory();

export const goToLastPageVisited = () => {
  history.push(
    localStorage.getItem("page") !== null ? localStorage.getItem("page") : "/"
  );
};
