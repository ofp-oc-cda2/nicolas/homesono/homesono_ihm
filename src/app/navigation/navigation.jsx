import React from "react";
import { Link } from "react-router-dom";
import "./navigation.scss";

const NavigationComponent = () => {
  return (
    <>
      <div className="navigation">
        <h1>Navigation</h1>
        <nav>
          <Link to="/">Home</Link>
        </nav>
      </div>
    </>
  );
};

export default NavigationComponent;
