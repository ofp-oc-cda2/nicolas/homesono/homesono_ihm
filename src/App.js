import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  useRouteMatch,
  useParams,
} from "react-router-dom";
import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css";
import Header from "./app/views/header/header";
import HomePage from "./app/views/homepage/homepage";
import Categories from "./app/views/categories/categories";
import Products from "./app/views/products/products";
import Product from "./app/views/product/product";
import Users from "./app/views/users/users";
import "./App.scss";

export default function App() {
  return (
    <>
      <Header />
      <Router>
        <div>
          <ul className="navBar">
            <li>
              <NavLink to="/">
                <i className="pi pi-home" style={{ fontSize: "2em" }}></i>
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/users"
                activeStyle={{
                  fontWeight: "bold",
                  color: "red",
                }}
              >
                Utilisateurs
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/categories"
                activeStyle={{
                  fontWeight: "bold",
                  color: "red",
                }}
              >
                Catégories
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/products"
                activeStyle={{
                  fontWeight: "bold",
                  color: "red",
                }}
              >
                Produits
              </NavLink>
            </li>
          </ul>

          <Switch>
            <Route path="/users">
              <Users />
            </Route>
            <Route path="/categories">
              <Categories />
            </Route>
            <Route path="/products">
              <Products />
            </Route>
            <Route path="/product">
              <Product />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </div>
      </Router>
    </>
  );
}

function Topic() {
  let { topicId } = useParams();
  return <h3>Requested topic ID: {topicId}</h3>;
}
